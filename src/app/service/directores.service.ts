import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class DirectoresService {
  constructor(private http: HttpClient) {}

  directoresSelect() {
    return this.http.get(environment.campus.directores);
  }

  directoresInsert(nombres: string, peliculas: string) {
    const formData: FormData = new FormData();
    formData.append("nombres", nombres);
    formData.append("peliculas", peliculas);
    return this.http.post(environment.campus.directoresInsert, formData).pipe(
      map((res) => {
        return res;
      })
    );
  }

  directoresUpdate(iddirector: string, nombres: string, peliculas: string) {
    const formData: FormData = new FormData();
    formData.append("nombres", nombres);
    formData.append("peliculas", peliculas);
    formData.append("iddirector", iddirector);
    return this.http.post(environment.campus.directoresUpdate, formData).pipe(
      map((res) => {
        return res;
      })
    );
  }

  directoresDelete(iddirector: string) {
    const formData: FormData = new FormData();
    formData.append("iddirector", iddirector);
    return this.http.post(environment.campus.directoresDelete, formData).pipe(
      map((res) => {
        return res;
      })
    );
  }

}
