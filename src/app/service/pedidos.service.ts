import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class PedidosService {
	
  constructor(private http: HttpClient) {}

  pedidosSelect() {
    return this.http.get(environment.campus.pedidos);
  }

	pedidoDetallesSelect(idpedido: any){
    return this.http.get(environment.campus.pedidosDetalle + idpedido);
  }
	
}
