import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  constructor(private http: HttpClient) { }

  productosSelect(idcategoria: any){
    return this.http.get(environment.campus.productoCategoria + idcategoria);
  }
  productoDetallesSelect(idproducto: any){
    return this.http.get(environment.campus.productoDetalle + idproducto);
  }
}
