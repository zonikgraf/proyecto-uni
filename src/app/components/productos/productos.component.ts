import { Component, Input } from '@angular/core';
import { Categoria } from 'src/app/entities/categoria';
import { ItemCarrito } from 'src/app/entities/itemcarrito';
import { Producto } from 'src/app/entities/producto';
import { ProductoDetalle } from 'src/app/entities/productodetalle';
import { ProductosService } from 'src/app/service/productos.service';

@Component({
  selector: "app-productos",
  templateUrl: "./productos.component.html",
  styleUrls: ["./productos.component.css"],
})
export class ProductosComponent {
  precarga: Boolean = true;
  @Input() categoriaX: Categoria | undefined;

  listaProductos: Producto[] = [];
  listaProductoDetalles: ProductoDetalle[] = [];

  constructor(private productoService: ProductosService) {}

  ngOnChanges(): void {
    console.log(this.categoriaX);
		this.precarga = true;
    if (this.categoriaX !== undefined) {
      this.productoService
        .productosSelect(this.categoriaX?.idcategoria)
        .subscribe((res) => {
          console.log(res);
          this.listaProductos = JSON.parse(JSON.stringify(res));
					this.precarga = false;
        });
    }
  }

  agregarCarrito(productoSeleccionado: Producto) {
    console.log(productoSeleccionado);
    let itemCarrito: ItemCarrito = {
      idproducto: productoSeleccionado.idproducto,
      nombre: productoSeleccionado.nombre,
      precio:
        productoSeleccionado.preciorebajado == 0
          ? productoSeleccionado.precio
          : productoSeleccionado.preciorebajado,
      cantidad: 1,
    };
    let carrito: ItemCarrito[] = [];
    if (sessionStorage.getItem("carritocompras") === null) {
      carrito.push(itemCarrito);
      sessionStorage.setItem("carritocompras", JSON.stringify(carrito));
    } else {

      let carritoStorage = sessionStorage.getItem("carritocompras") as string;
      carrito = JSON.parse(carritoStorage);
			
      let index = -1;
      for (let i = 0; i < carrito.length; i++) {
        if (itemCarrito.idproducto === carrito[i].idproducto) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        carrito.push(itemCarrito);
        sessionStorage.setItem("carritocompras", JSON.stringify(carrito));
      } else {
        carrito[index].cantidad!++;
        sessionStorage.setItem("carritocompras", JSON.stringify(carrito));
      }

    }
  }

  mostrarDatosVistaRapida(idproducto: string) {
    console.log(idproducto);
    this.productoService.productoDetallesSelect(idproducto).subscribe((res) => {
      console.log(res);
      this.listaProductoDetalles = JSON.parse(JSON.stringify(res));
    });
  }
}
