import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Director } from 'src/app/entities/director';
import { DirectoresService } from 'src/app/service/directores.service';

@Component({
  selector: "app-directores",
  templateUrl: "./directores.component.html",
  styleUrls: ["./directores.component.css"],
})
export class DirectoresComponent {
  precarga: Boolean = true;
  listaDirectores: Director[] = [];
  editarDirector: boolean = false;
	directorSeleccionado!: Director;

  directoresInsertForm = new FormGroup({
    nombres: new FormControl(),
    peliculas: new FormControl(),
  });

  directoresUpdateForm = new FormGroup({
    iddirector: new FormControl(),
    nombres: new FormControl(),
    peliculas: new FormControl(),
  });

  constructor(private directoresService: DirectoresService) {}

  ngOnInit(): void {
    this.directoresService.directoresSelect().subscribe((res) => {
      console.log(res);
      this.listaDirectores = JSON.parse(JSON.stringify(res));
      this.precarga = false;
    });
  }

  directoresInsert(values: any, modal?: any) {
    console.log(values);
    this.directoresService
      .directoresInsert(values.nombres, values.peliculas)
      .subscribe((res) => {
        console.log(res);
        this.ngOnInit();
        this.directoresInsertForm.reset();
        const btn = document.getElementById("cerrarModalInsert");
        if (btn) btn.click();
      });
  }

  directoresUpdate(values: any, modal?: any) {
    console.log(values);
    this.directoresService
      .directoresUpdate(values.iddirector, values.nombres, values.peliculas)
      .subscribe((res) => {
        console.log(res);
        this.ngOnInit();
        this.directoresInsertForm.reset();
        const btn = document.getElementById("cerrarModalUpdate");
        if (btn) btn.click();
      });
  }

  directoresDelete(iddirector: any) {
    this.directoresService.directoresDelete(iddirector).subscribe((res) => {
      console.log(res);
      this.ngOnInit();
      const btn = document.getElementById("cerrarModalDelete");
      if (btn) btn.click();
    });
  }

  editDirector(item: Director, editar: boolean) {
    console.log(item);
    this.editarDirector = editar;
    this.directoresUpdateForm.setValue({
      iddirector: item.iddirector,
      nombres: item.nombres,
      peliculas: item.peliculas,
    });
  }

  deletDirector(director: Director) {
		this.directorSeleccionado = director;
  }
}
