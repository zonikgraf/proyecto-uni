import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PedidoDetalle } from 'src/app/entities/pedidodetalle';
import { PedidosService } from 'src/app/service/pedidos.service';

@Component({
  selector: "app-pedido-detalles",
  templateUrl: "./pedido-detalles.component.html",
  styleUrls: ["./pedido-detalles.component.css"],
})
export class PedidoDetallesComponent {
  precarga: Boolean = true;
  listaPedidos: PedidoDetalle[] = [];
	idPedido: string = '00000';

  constructor(
    private activateRoute: ActivatedRoute,
    private pedidoService: PedidosService
  ) {}

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params) => {
      const idpedido = params["idpedido"];
      console.log(idpedido);
      this.leerPedido(idpedido);
    });
  }

  leerPedido(idpedido: String) {
		this.idPedido = String(idpedido);
    this.pedidoService.pedidoDetallesSelect(idpedido).subscribe((res) => {
      console.log(res);
      this.listaPedidos = JSON.parse(JSON.stringify(res));
      this.precarga = false;
    });
  }

}
