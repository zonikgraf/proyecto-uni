import { Component } from '@angular/core';
import { Empleado } from 'src/app/entities/empleado';

@Component({
  selector: "app-ganadores",
  templateUrl: "./ganadores.component.html",
  styleUrls: ["./ganadores.component.css"],
})
export class GanadoresComponent {
  listaItemsGanadores: Empleado[] = [];

  ngOnInit(): void {
    this.obtenerListaGanadores();
  }

  obtenerListaGanadores() {
    let ganadoresStorage = sessionStorage.getItem("ganadores") as string;
    let ganadores = JSON.parse(ganadoresStorage);
    this.listaItemsGanadores = ganadores;
		if ( !this.listaItemsGanadores ) this.listaItemsGanadores = [];
  }

  vaciarGanadores() {
    this.listaItemsGanadores = [];
    sessionStorage.removeItem("ganadores");
  }

}
