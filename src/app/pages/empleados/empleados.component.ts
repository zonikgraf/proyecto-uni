import { Component } from '@angular/core';
import { Empleado } from 'src/app/entities/empleado';
import { EmpleadosService } from 'src/app/service/empleados.service';

@Component({
  selector: "app-empleados",
  templateUrl: "./empleados.component.html",
  styleUrls: ["./empleados.component.css"],
})
export class EmpleadosComponent {
  precarga: Boolean = true;
  listaEmpleados: Empleado[] = [];
  empleadoEncontrado: boolean = false;
  constructor(private empleadoService: EmpleadosService) {}

  ngOnInit(): void {
    this.empleadoService.empleadosSelect().subscribe((res) => {
      console.log(res);
      this.listaEmpleados = JSON.parse(JSON.stringify(res));
      this.precarga = false;
    });
  }

  agregarGanador(empleadoSeleccionado: Empleado) {
    this.empleadoEncontrado = false;

    let itemEmpleado: Empleado = {
      nombres: empleadoSeleccionado.nombres,
      apellidos: empleadoSeleccionado.apellidos,
      foto: empleadoSeleccionado.foto,
      idempleado: empleadoSeleccionado.idempleado,
      cargo: empleadoSeleccionado.cargo,
    };

    let listaGanadores: Empleado[] = [];

    if (sessionStorage.getItem("ganadores") === null) {

      listaGanadores.push(itemEmpleado);
      sessionStorage.setItem("ganadores", JSON.stringify(listaGanadores));

    } else {

      let ganadoresStorage = sessionStorage.getItem("ganadores") as string;
      listaGanadores = JSON.parse(ganadoresStorage);
      for (let i = 0; i < listaGanadores.length; i++) {
        if (listaGanadores[i].idempleado === itemEmpleado.idempleado) {
          this.empleadoEncontrado = true;
          break;
        }
      }
			if (!this.empleadoEncontrado) {
				listaGanadores.push(itemEmpleado);
        sessionStorage.setItem("ganadores", JSON.stringify(listaGanadores));
      }
			
    }
  }

  cerrarAlerta() {
		this.empleadoEncontrado = false;
	}

}
