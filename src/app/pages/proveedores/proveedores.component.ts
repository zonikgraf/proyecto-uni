import { Component } from '@angular/core';
import { Proveedor } from 'src/app/entities/proveedor';
import { ProveedoresService } from 'src/app/service/proveedores.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ProveedoresComponent {
  precarga: Boolean = true;
  listaProveedores: Proveedor[] = [];
  proveedoresFilter: any = { nombreempresa: '' }
  numeroPagina: number = 1;

  constructor( private proveedorService: ProveedoresService){}

  ngOnInit(): void {
    this.proveedorService.proveedoresSelect().subscribe(
      (res) =>{
        console.log(res)
        this.listaProveedores = JSON.parse(JSON.stringify(res))
        this.precarga = false;
      }
    )
  }
}
