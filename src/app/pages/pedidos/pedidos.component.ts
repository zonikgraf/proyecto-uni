import { Component } from '@angular/core';
import { Pedido } from 'src/app/entities/pedido';
import { PedidosService } from 'src/app/service/pedidos.service';

@Component({
  selector: "app-pedidos",
  templateUrl: "./pedidos.component.html",
  styleUrls: ["./pedidos.component.css"],
})
export class PedidosComponent {
  precarga: Boolean = true;
  listaPedidos: Pedido[] = [];
  pedidoeleccionado: Pedido | undefined;
  numeroPagina: number = 1;

  constructor(private pedidosService: PedidosService) {}

  ngOnInit(): void {
    this.obtenerPedidos();
  }

	obtenerPedidos() {
		this.pedidosService.pedidosSelect().subscribe((res) => {
      console.log(res);
      this.listaPedidos = JSON.parse(JSON.stringify(res));
      this.precarga = false;
    });
	}

}
