export class PedidoDetalle {
  idpedido: string | undefined;
  idproducto: string | undefined;
  precio: string | undefined;
  cantidad: string | undefined;
  nombre: string | undefined;
  detalle: string | undefined;
  imagenchica: string | undefined;
}
