export class Pedido {
  idpedido: string | undefined;
  fechapedido: string | undefined;
  usuario: string | undefined;
  nombres: string | undefined;
  total: string | undefined;
}
