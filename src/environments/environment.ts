
export const url = "https://servicios.campus.pe/";

export const environment = {
  production: false,
  campus: {
    categorias: `${url}categorias.php`,
    directores: `${url}directores.php`,
    directoresInsert: `${url}directoresinsert.php`,
    directoresUpdate: `${url}directoresupdate.php`,
    directoresDelete: `${url}directoresdelete.php`,
    empleados: `${url}empleados.php`,
    envios: `${url}servicioenvios.php`,
    pedidos: `${url}pedidos.php`,
    pedidosDetalle: `${url}pedidosdetalle.php?idpedido=`,
    productoCategoria: `${url}productos.php?idcategoria=`,
    productoDetalle: `${url}productos.php?idproducto=`,
    proveedores: `${url}proveedores.php`,
  },
};